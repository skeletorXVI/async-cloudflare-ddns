#!/usr/bin/env python

"""
Use cloudflare as a dynamic dns provider. Configuration is done via
environment variables. The script is intended to be used in a dockerized environment.

Inspired by https://github.com/timothymiller/cloudflare-ddns.
All configuration has been moved to environment variables to allow setting up a
container from within portainer.
"""

# BSD 3-Clause License
#
# Copyright (c) 2021, Fabian Haenel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import asyncio
import logging
import os
import re
import sys
from datetime import datetime
from typing import TypeVar, Generic, Optional

import aiohttp
import yaml

from pydantic import BaseModel, Extra, Field
from pydantic.generics import GenericModel


__version__ = "0.1.0"

__license__ = "BSD-3-Clause"
__copyright__ = "Copyright 2021, Fabian Haenel"

__author__ = "Fabian Haenel"
__credits__ = ["Fabian Haenel"]
__maintainer__ = "Fabian Haenel"
__email__ = "skeletorxvi@gmail.com"


# Exit codes
EC_COMPLETED = 0
EC_ERROR = 1
EC_NO_CONFIGURATION = 2
EC_INVALID_CONFIGURATION = 3
EC_CONFIGURATION_PARSING_ERROR = 4

# Environment variable keys
LOG_LEVEL_KEY = "DDNS_LOG_LEVEL"
ZONES_KEY = "DDNS_ZONES"
LOOP_DURATION_KEY = "DDNS_LOOP_DURATION"

# Minimum is set to lowest ttl of cloudflare
MINIMUM_LOOP_DURATION = 120
DEFAULT_LOOP_DURATION = 300

# Cloudflare constants
CLOUDFLARE_BASE_URL = "https://api.cloudflare.com/client/v4"
CLOUDFLARE_DNS_RECORDS_PER_PAGE = 100
CLOUDFLARE_DNS_RECORD_TTL = 300
CLOUDFLARE_DNS_RECORD_PRIORITY = 10

# Public ip stuff
PUBLIC_IP_PROVIDER = "https://www.cloudflare.com/cdn-cgi/trace"
ip_v4_regex = re.compile(r"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}")

# logger
logger = logging.Logger("async-cloudflare-ddns")


T = TypeVar("T")


class PublicIpRequestException(Exception):
    def __init__(self, status_code: int):
        super().__init__()
        self.status_code = status_code


class CloudflareRequestException(Exception):
    def __init__(self, status_code: int, json: dict):
        super().__init__()
        self.errors = json["errors"]
        self.status_code = status_code
        self.json = json


class ZoneConfig(BaseModel):
    zone_id: str
    api_token: str
    domains: dict[str, bool]
    enterprise_features: bool = False


class Config(BaseModel):
    zones: list[ZoneConfig]
    loop_duration: int


class PublicIpResponse(BaseModel):
    host: str = Field(alias="h")
    ip: str

    class Config:
        extra = Extra.ignore


class CloudflareError(BaseModel):
    code: int
    message: str


class CloudflarePaginator(BaseModel):
    page: int
    per_page: int
    count: int
    total_count: int
    total_pages: int


class CloudflareResponse(GenericModel, Generic[T]):
    success: bool
    errors: list[CloudflareError]
    messages: list[str]
    result: T


class PaginatedCloudflareResponse(CloudflareResponse[T], Generic[T]):
    result_info: Optional[CloudflarePaginator]


class CloudflareDnsRecord(BaseModel):
    id: str
    type: str
    name: str
    content: str
    proxiable: bool
    proxied: bool
    ttl: int
    locked: bool
    zone_id: str
    zone_name: str
    created_on: datetime
    modified_on: datetime
    data: Optional[dict]
    meta: dict


def _string_to_log_level(value: str) -> int:
    """
    Parses string to log level. Casing will be converted to upper before parsing.

    :param value: The string to parse
    :raises ValueError: If the string is not a valid log level
    :return: The integer value for the log level
    """

    loglevel = value.upper()
    if loglevel not in {"DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"}:
        raise ValueError()

    return getattr(logging, loglevel)


def _configure_logging():
    """
    Configures the logger to to print to terminal. Sets the log level based on the
    environment variable DDNS_LOG_LEVEL or to INFO if the variable does not exist
    or does not contain a valid value.
    """

    # Create console handler and set level to debug
    console_handler = logging.StreamHandler()

    # Create formatter
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)8s - %(message)s"
    )

    # Add formatter to console handler
    console_handler.setFormatter(formatter)

    # Add console handler to logger
    logger.addHandler(console_handler)

    # Set log level
    try:
        log_level = _string_to_log_level(os.environ[LOG_LEVEL_KEY])
    except (KeyError, ValueError):
        log_level = logging.INFO
    logger.setLevel(log_level)
    logger.info("Log level is set to %s", logging.getLevelName(log_level))


def load_config() -> Config:
    """
    Loads configuration from DDNS_CONFIG environment variable.
    The environment variable is expected to contain a yaml list where each entry has
    the fields zone_id, api_token and domains.

    zone_id:   the cloudflare zone id
    api_token: the cloudflare api token with "Edit zone DNS" permissions for the zone
    domains:   mapping where the keys are the domains and the values a boolean value
               that determines whether to enable cloudflare proxy for that domain

    Example:
    - zone_id: a4a44a44aaaa44444a444aaa4444444a
      api_token: 2x2xxxxxxxxxxxxxxxxxx2xxxx-xxxxxxx_xx2xx
      domains:
        example.com: true  # Enable proxying for domain
        test.example.com: false  # Disable proxying for domain

    :return: Returns the config list
    """

    logger.debug("Loading loop duration from environment variables...")
    loop_duration = os.environ.get(LOOP_DURATION_KEY, DEFAULT_LOOP_DURATION)
    if not isinstance(loop_duration, int):
        logger.critical("Loop duration needs to be an integer")
        sys.exit(EC_INVALID_CONFIGURATION)

    if loop_duration < MINIMUM_LOOP_DURATION:
        logger.warning(
            "Loop duration set to low, correcting to minimum of %i seconds",
            MINIMUM_LOOP_DURATION,
        )
        loop_duration = MINIMUM_LOOP_DURATION
    else:
        logger.info("Loop duration set to %i seconds", loop_duration)

    try:
        logger.debug("Loading zone configurations from environment variables...")
        raw_zone_config = os.environ[ZONES_KEY]
        logger.info("Loaded zone configurations from environment variables")

        logger.debug("Parsing zone configurations...")
        config = yaml.safe_load(raw_zone_config)
        logger.info("Parsed zone configurations")

        logger.debug("Converting configuration into object...")
        return Config(
            loop_duration=loop_duration,
            zones=[ZoneConfig(**entry) for entry in config],
        )
    except KeyError:
        logger.critical(
            "The configuration environment variable %s is not set", ZONES_KEY
        )
        sys.exit(EC_NO_CONFIGURATION)
    except yaml.YAMLError as e:
        logger.critical("The configuration could not be parsed -> %s", e)
        sys.exit(EC_CONFIGURATION_PARSING_ERROR)


def stringify_cloudflare_errors(exception: CloudflareRequestException):
    return " | ".join(
        f"{error['code']}: {error['message']}" for error in exception.errors
    )


def determine_record_type_from_ip(ip: str) -> str:
    return "AAAA" if ip_v4_regex.search(ip) is None else "A"


async def get_public_ip() -> PublicIpResponse:
    async with aiohttp.ClientSession() as session:
        async with session.get(PUBLIC_IP_PROVIDER) as response:
            if response.status != 200:
                raise PublicIpRequestException(response.status)

            raw_text = await response.text()
            return PublicIpResponse(
                **dict(line.split("=", 1) for line in raw_text.strip("\n").split("\n"))
            )


async def list_dns_records(
    api_token: str,
    zone_identifier: str,
    record_type: str,
    page: int,
) -> PaginatedCloudflareResponse[list[CloudflareDnsRecord]]:
    async with aiohttp.ClientSession() as session:
        headers = {"Authorization": f"Bearer {api_token}"}
        query_params = {
            "per_page": CLOUDFLARE_DNS_RECORDS_PER_PAGE,
            "page": page,
            "type": record_type,
        }
        async with session.get(
            f"{CLOUDFLARE_BASE_URL}/zones/{zone_identifier}/dns_records",
            headers=headers,
            params=query_params,
        ) as response:
            response_json = await response.json()

            if response.status != 200:
                raise CloudflareRequestException(response.status, response_json)

            return PaginatedCloudflareResponse[list[CloudflareDnsRecord]](
                **response_json
            )


async def list_all_dns_records(
    api_token: str, zone_identifier: str, record_type: str
) -> list[CloudflareDnsRecord]:
    logger.debug(
        "Requesting first page of dns records for zone %s and type %s",
        zone_identifier,
        record_type,
    )
    first_page = await list_dns_records(api_token, zone_identifier, record_type, 1)
    logger.debug(
        "Received %i dns records on first page for zone %s and type %s,"
        " in total there are %i entries on %i pages",
        first_page.result_info.count,
        zone_identifier,
        record_type,
        first_page.result_info.total_count,
        first_page.result_info.total_pages,
    )

    pages = [first_page]

    if first_page.result_info.total_pages > 1:
        logger.debug(
            "Requesting remaining pages of dns records for zone %s and type %s...",
            zone_identifier,
            record_type,
        )
        following_pages_requests = [
            list_dns_records(api_token, zone_identifier, record_type, i)
            for i in range(2, first_page.result_info.total_pages + 1)
        ]

        pages.extend(await asyncio.gather(*following_pages_requests))
        logger.debug(
            "Received remaining pages of dns records for zone %s and type %s",
            zone_identifier,
            record_type,
        )

    return [entry for result in pages for entry in result.result]


async def create_dns_record(
    api_token: str,
    zone_identifier: str,
    domain: str,
    ip: str,
    record_type: str,
    proxied: bool,
) -> CloudflareResponse[CloudflareDnsRecord]:
    async with aiohttp.ClientSession() as session:
        headers = {"Authorization": f"Bearer {api_token}"}
        content = {
            "type": record_type,
            "name": domain,
            "content": ip,
            "proxied": proxied,
            "ttl": CLOUDFLARE_DNS_RECORD_TTL,
            "priority": CLOUDFLARE_DNS_RECORD_PRIORITY,
        }
        async with session.post(
            f"{CLOUDFLARE_BASE_URL}/zones/{zone_identifier}/dns_records",
            headers=headers,
            json=content,
        ) as response:
            response_json = await response.json()

            if response.status != 200:
                raise CloudflareRequestException(response.status, response_json)

            return CloudflareResponse[CloudflareDnsRecord](**response_json)


async def _create_dns_record(
    api_token: str,
    zone_identifier: str,
    domain: str,
    ip: str,
    record_type: str,
    proxied: bool,
):
    logger.debug(
        "Creating %s dns record for %s with ip %s and %s proxy in zone %s...",
        record_type,
        domain,
        ip,
        "enabled" if proxied else "disabled",
        zone_identifier,
    )
    try:
        response = await create_dns_record(
            api_token,
            zone_identifier,
            domain,
            ip,
            record_type,
            proxied,
        )

        logger.info(
            "Created %s dns record %s for %s with ip %s and %s proxy in zone %s",
            response.result.type,
            response.result.id,
            response.result.name,
            response.result.content,
            "enabled" if proxied else "disabled",
            zone_identifier,
        )
    except CloudflareRequestException as e:
        logger.error(
            "Failed to create %s dns record for %s with ip %s and %s proxy in zone %s"
            " -> %s",
            record_type,
            domain,
            ip,
            "enabled" if proxied else "disabled",
            zone_identifier,
            stringify_cloudflare_errors(e),
        )


async def patch_dns_record(
    api_token: str, zone_identifier: str, record_identifier: str, ip: str, proxied: bool
) -> CloudflareResponse[CloudflareDnsRecord]:
    async with aiohttp.ClientSession() as session:
        headers = {"Authorization": f"Bearer {api_token}"}
        content = {
            "content": ip,
            "proxied": proxied,
            "ttl": CLOUDFLARE_DNS_RECORD_TTL,
            "priority": CLOUDFLARE_DNS_RECORD_PRIORITY,
        }
        async with session.patch(
            f"{CLOUDFLARE_BASE_URL}/zones/{zone_identifier}/dns_records/{record_identifier}",
            headers=headers,
            json=content,
        ) as response:
            response_json = await response.json()

            if response.status != 200:
                raise CloudflareRequestException(response.status, response_json)

            return CloudflareResponse[CloudflareDnsRecord](**response_json)


async def _patch_dns_record(
    api_token: str, zone_identifier: str, record_identifier: str, ip: str, proxied: bool
):
    logger.debug(
        "Patching dns record %s to ip %s and %s proxy in zone %s...",
        record_identifier,
        ip,
        "enable" if proxied else "disable",
        zone_identifier,
    )
    try:
        response = await patch_dns_record(
            api_token,
            zone_identifier,
            record_identifier,
            ip,
            proxied,
        )

        logger.info(
            "Patched %s dns record %s for %s with ip %s and %s proxy in zone %s",
            response.result.type,
            response.result.id,
            response.result.name,
            response.result.content,
            "enabled" if proxied else "disabled",
            zone_identifier,
        )
    except CloudflareRequestException as e:
        logger.error(
            "Failed to patch dns record %s with ip %s and %s proxy in zone %s -> %s",
            record_identifier,
            ip,
            "enabled" if proxied else "disabled",
            zone_identifier,
            stringify_cloudflare_errors(e),
        )


async def update_zone(zone_config: ZoneConfig, public_ip: str):
    record_type = determine_record_type_from_ip(public_ip)
    try:
        dns_records = await list_all_dns_records(
            zone_config.api_token, zone_config.zone_id, record_type
        )
    except CloudflareRequestException as e:
        logger.error(
            "Failed to retrieve dns records for zone %s -> %s",
            zone_config.zone_id,
            stringify_cloudflare_errors(e),
        )
        return

    logger.debug("Mapping existing dns records for zone %s", zone_config.zone_id)
    dns_records_mapping = {
        dns_record.name: dns_record
        for dns_record in dns_records
        if dns_record.name in zone_config.domains
    }
    logger.debug("Mapped existing dns records for zone %s", zone_config.zone_id)

    # The first tuple value is the record id and the second determines proxying
    records_to_patch = []
    # The first tuple value is the domain to create and the second determines proxying
    records_to_create = []

    skipped_counter = 0

    for domain, proxied in zone_config.domains.items():
        if not zone_config.enterprise_features and proxied and domain.startswith("*"):
            logger.warning(
                "Wildcard domain %s can only be proxied if enterprise features are"
                " enabled, disabling proxy",
                domain,
            )
            proxied = False

        if domain in dns_records_mapping:
            dns_record = dns_records_mapping[domain]

            if dns_record.content == public_ip and dns_record.proxied == proxied:
                logger.info("Found up-to-date dns record for domain %s", domain)
                skipped_counter += 1
            else:
                logger.info("Found outdated dns record for domain %s", domain)
                records_to_patch.append(
                    _patch_dns_record(
                        zone_config.api_token,
                        zone_config.zone_id,
                        dns_record.id,
                        public_ip,
                        proxied,
                    )
                )
        else:
            logger.info("No dns record found for domain %s", domain)
            records_to_create.append(
                _create_dns_record(
                    zone_config.api_token,
                    zone_config.zone_id,
                    domain,
                    public_ip,
                    record_type,
                    proxied,
                )
            )

    logger.info(
        "Skipped %i, creating %i and patching %i dns records",
        skipped_counter,
        len(records_to_create),
        len(records_to_patch),
    )
    await asyncio.gather(*records_to_create + records_to_patch)


async def loop(config: Config):
    while True:
        logger.info("Starting update cycle")
        try:
            public_ip = await get_public_ip()
            await asyncio.gather(
                *(
                    update_zone(zone_config, public_ip.ip)
                    for zone_config in config.zones
                )
            )
        except PublicIpRequestException as e:
            logger.error(
                "Failed to retrieve public ip, skipping update cycle -> %i",
                e.status_code,
            )

        logger.info(
            "Finished update cycle -> waiting for %i seconds", config.loop_duration
        )
        await asyncio.sleep(config.loop_duration)


async def main():
    _configure_logging()
    logger.info(
        "Running async cloudflare dynamic dns script version %s by %s",
        __version__,
        __author__,
    )
    config = load_config()
    await loop(config)


if __name__ == "__main__":
    try:
        asyncio.get_event_loop().run_until_complete(main())
    except KeyboardInterrupt:
        logger.info("Received exit signal, exiting application")
