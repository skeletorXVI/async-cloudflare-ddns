# Async Cloudflare DDNS

A simple-to-use docker container to use cloudflare as a dynamic dns provider. 
Configuration is done via environment variables.

Inspired by [Cloudflare DDNS](https://github.com/timothymiller/cloudflare-ddns).
All configurations have been moved to environment variables to allow setting up a
container from within portainer.

## Configuration

```
DDNS_LOG_LEVEL
```

**Determines the log level.** 

_Optional_

Valid values (<small>case does not matter</small>): `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`

Default: `INFO`

```
DDNS_LOOP_DURATION
```

**The loop duration in seconds. Minimum is 120 seconds.**

_Optional_

Default: `300`

```
DDNS_ZONES
```

**The list of zone configurations for domains to configure via dynamic dns**

_Required_

A single zone entry:

```yaml
# The zone id can be found on the overview in cloudflare
zone_id: b1b11b11bbbb11111b111bbb1111111b

# Only api tokens are supported, create a token with "Edit zone DNS" permissions 
# See https://developers.cloudflare.com/api/tokens/create
api_token: 3G3GGGhhGGhhhGGhhhGhh3hhGh-hGGhhhG_hG3GG

# Enable enterprise features, this option is optional and false by default
# Enterprise features are required to proxy wildcard domains
enterprise_features: false

# A mapping of the domains to manage and if proxying for the domain should be enabled
domains:
  example.com: true
  staging.example.com: false
  # Wildcard domains are supported.
  # Make sure to surround the domain with quotation marks as * is used for yaml anchors
  # Proxying will be turned off no matter the declared value, unless enterprise features are enabled
  "*.example.com": true
```

Full example

```yaml
- zone_id: b1b11b11bbbb11111b111bbb1111111b
  api_token: 3G3GGGhhGGhhhGGhhhGhh3hhGh-hGGhhhG_hG3GG
  domains:
    example.com: true
    staging.example.com: true
- zone_id: f1f11f11ffff11111f111fff1111111f
  api_token: 3G3GGGhhGGhhhGGhhhGhh3hhGh-hGGhhhG_hG3GG
  enterprise_features: true
  domains:
    beispiel.de: true
    "*.beispiel.de": true
```

## docker-compose example

```yaml
version: "3"

services:
  ddns-client:
    image: registry.gitlab.com/skeletorxvi/async-cloudflare-ddns:0.1.0
    restart: unless-stopped
    environment:
      DDNS_ZONES: |
        - zone_id: b1b11b11bbbb11111b111bbb1111111b
          api_token: 3G3GGGhhGGhhhGGhhhGhh3hhGh-hGGhhhG_hG3GG
          domains:
            example.com: true
            staging.example.com: true
        - zone_id: f1f11f11ffff11111f111fff1111111f
          api_token: 3G3GGGhhGGhhhGGhhhGhh3hhGh-hGGhhhG_hG3GG
          enterprise_features: true
          domains:
            beispiel.de: true
            "*.beispiel.de": true
```

## IPv6

The script should work with IPv6. But I neither tested the script, nor the container
regarding IPv6 as I do not have a IPv6 address. 
